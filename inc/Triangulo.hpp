#ifndef TRIANGULO_HPP
#define TRIANGULO_HPP

#include "FormaGeometrica.hpp"

using namespace std;
class Triangulo: public FormaGeometrica {
private:
    float lateral1;
    float lateral2;
public:
    Triangulo();
    Triangulo(float base, float altura, float lateral1, float lateral2);
    ~Triangulo();

    void set_lateral1(float lateral1);
    float get_lateral1();

    void set_lateral2(float lateral2);
    float get_lateral2();

    float calcula_area();
    float calcula_perimetro();
};

#endif