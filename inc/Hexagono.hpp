#ifndef HEXAGONO_HPP
#define HEXAGONO_HPP

#include "FormaGeometrica.hpp"

using namespace std;

class Hexagono: public FormaGeometrica {
private:
    float apotema;
public:
    Hexagono();
    Hexagono(float base, float apotema);
    ~Hexagono();

    void set_apotema(float apotema);
    float get_apotema();
    float calcula_area();
    float calcula_perimetro();
};

#endif