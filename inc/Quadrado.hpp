#ifndef QUADRADO_HPP
#define QUADRADO_HPP

#include "FormaGeometrica.hpp"

using namespace std;

class Quadrado: public FormaGeometrica {
public:
    Quadrado();
    Quadrado(float base, float altura);
    ~Quadrado();
};

#endif