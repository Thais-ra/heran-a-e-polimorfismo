#ifndef PARALELOGRAMO_HPP
#define PARALELOGRAMO_HPP

#include "FormaGeometrica.hpp"

using namespace std;

class Paralelogramo: public FormaGeometrica{
    private:
        float laterais;
    public:
        Paralelogramo();
        Paralelogramo(float base, float altura, float laterais);
        ~Paralelogramo();

        void set_laterais(float laterais);
        float get_laterais();
        float calcula_perimetro();
};

#endif