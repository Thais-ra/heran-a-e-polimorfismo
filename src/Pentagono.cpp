#include "Pentagono.hpp"
#include <iostream>

Pentagono::Pentagono(){
    set_base(0.0f);
    apotema = 0.0f;
}
Pentagono::Pentagono(float base, float apotema){
    set_tipo("Pentágono");
    set_base(base);
    set_apotema(apotema);
}
Pentagono::~Pentagono(){
    cout << "Destruindo o objeto: " << get_tipo() << endl;
}
void Pentagono::set_apotema(float apotema){
    this->apotema = apotema;
}
float Pentagono::get_apotema(){
    return apotema;
}
float Pentagono::calcula_area(){
    return (calcula_perimetro() * get_apotema())/2;
}
float Pentagono::calcula_perimetro(){
    return 5*get_base();
}
