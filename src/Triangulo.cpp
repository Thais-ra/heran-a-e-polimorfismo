#include "Triangulo.hpp"
#include <iostream>

Triangulo::Triangulo(){
    set_base(0.0f);
    set_altura(0.0f);
    lateral1 = 0.0f;
    lateral2 = 0.0f;
}
Triangulo::Triangulo(float base, float altura, float lateral1, float lateral2){
    set_tipo("Triangulo");
    set_base(base);
    set_altura(altura);
    set_lateral1(lateral1);
    set_lateral2(lateral2);
}
Triangulo::~Triangulo(){
    cout << "Destruindo o objeto: " << get_tipo() << endl;
}
void Triangulo::set_lateral1(float lateral1){
    this->lateral1 = lateral1;
}
float Triangulo::get_lateral1(){
    return lateral1;
}
void Triangulo::set_lateral2(float lateral2){
    this->lateral2 = lateral2;
}
float Triangulo::get_lateral2(){
    return lateral2;
}
float Triangulo::calcula_area(){
    return (get_base() * get_altura()) /2;
}
float Triangulo::calcula_perimetro(){
    return get_base() + lateral1 + lateral2;
}
