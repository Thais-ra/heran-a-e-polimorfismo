#include "Hexagono.hpp"
#include <iostream>

Hexagono::Hexagono(){
    set_base(0.0f);
    apotema = 0.0f;
}
Hexagono::Hexagono(float base, float apotema){
    set_tipo("Hexágono");
    set_base(base);
    set_apotema(apotema);
}
Hexagono::~Hexagono(){
    cout << "Destruindo o objeto: " << get_tipo() << endl;
}
void Hexagono::set_apotema(float apotema){
    this->apotema = apotema;
}
float Hexagono::get_apotema(){
    return apotema;
}
float Hexagono::calcula_area(){
    return (calcula_perimetro()*get_apotema())/2;
}
float Hexagono::calcula_perimetro(){
    return 6*get_base();
}