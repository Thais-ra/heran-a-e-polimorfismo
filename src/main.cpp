#include <iostream>
#include <vector>

#include "Circulo.hpp"
#include "Hexagono.hpp"
#include "Paralelogramo.hpp"
#include "Pentagono.hpp"
#include "Quadrado.hpp"
#include "Triangulo.hpp"

using namespace std;

int main(){

    vector<FormaGeometrica*> lista;

    lista.push_back(new Circulo(2));
    lista.push_back(new Quadrado(5, 5));
    lista.push_back(new Triangulo(5, 3, 3, 4));
    lista.push_back(new Paralelogramo(3, 3, 5));
    lista.push_back(new Pentagono(4, 4));
    lista.push_back(new Hexagono(3, 3));

    for (FormaGeometrica *c: lista){
        c->imprime_dados();
        cout << endl;
    }
    return 0;
}