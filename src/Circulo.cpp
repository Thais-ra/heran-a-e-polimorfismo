#include "Circulo.hpp"
#include <iostream>
#include <cmath>

#define PI 3.14159265

Circulo::Circulo(){
    raio = 0.0f;
}
Circulo::Circulo(float raio){
    set_tipo("Circulo");
    this->raio = raio;
}
Circulo::~Circulo(){
    cout << "Destruindo o objeto: " << get_tipo() << endl;
}
void Circulo::set_raio(float raio){
    this->raio = raio;
}
float Circulo::get_raio(){
    return raio;
}
float Circulo::calcula_area(){
    return (pow(raio, 2)*PI);
}
float Circulo::calcula_perimetro(){
    return 2*(PI * raio);
}


