#include "Paralelogramo.hpp"
#include <iostream>

Paralelogramo::Paralelogramo(){
    set_base(0.0f);
    set_altura(0.0f);
    laterais = 0.0f;
}
Paralelogramo::Paralelogramo(float base, float altura, float laterais){
    set_tipo("Paralelogramo");
    set_base(base);
    set_altura(altura);
    set_laterais(laterais);
}
Paralelogramo::~Paralelogramo(){
    cout << "Destruindo o objeto: " << get_tipo() << endl;
}
void Paralelogramo::set_laterais(float laterais){
    this->laterais = laterais;
}
float Paralelogramo::get_laterais(){
    return laterais;
}
float Paralelogramo::calcula_perimetro(){
    return 2*laterais + 2*get_base();
}
